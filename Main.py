matriz = [
    [1, 0, 1],
    [0, 0, 1],
    [0, 1, 1]
]

posiciones = set()


def paresDeComponentes():
    for (x, y) in posiciones:
        print x, " - ", y


def ingresoDePixelesEnSet(X, Y):
    if posicionValida(X, Y):
        if matriz[X][Y] == 1:
            posiciones.add((X, Y))
            return True
    return False


def vecindadDePixel(X, Y):
    for a in [-1, 0, 1]:
        for b in [-1, 0, 1]:
            newX = X + a
            newY = Y + b
            yield newX, newY


def posicionValida(X, Y):
    if X >= 0 and Y >= 0 and X < len(matriz):
        if Y < len(matriz[X]):
            return True
    return False


def pixelesOn():
    X = 0
    for renglon in matriz:
        for Y in range(len(renglon)):
            if ingresoDePixelesEnSet(X, Y):
                for (newX, newY) in vecindadDePixel(X, Y):
                    ingresoDePixelesEnSet(newX, newY)
        X += 1


componentes = set()
componenteIndividual = set()


def validacionDeAdyacenciasEnPixeles(X, Y):
    componentes.clear()
    for (newX, newY) in vecindadDePixel(X, Y):
        for (x, y) in posiciones:
            if (newX, newY) == (x, y):
                componentes.add((x, y))


def cantidadComponentes():
    numeroDeComponentes = 0
    numeroDePosiciones = 0

    for (x, y) in posiciones:
        validacionDeAdyacenciasEnPixeles(x, y)
        numeroDePosiciones += len(componentes)
        if len(componentes) != 0:
            numeroDeComponentes += 1
        if len(posiciones) == numeroDePosiciones:
            componentes.clear()
            break

    print "Numero de Componentes=", numeroDeComponentes


def validacionPorComponente(X, Y):
    componenteIndividual.clear()
    for (newX, newY) in vecindadDePixel(X, Y):
        for (x, y) in posiciones:
            if (newX, newY) == (x, y):
                componenteIndividual.add((x, y))

numeroDePixelesPorComponente = set()

def numeroDePixelesEnComponentes():
    totalPosiciones = 0

    for (x, y) in posiciones:
        validacionPorComponente(x, y)
        pixeles = len(componenteIndividual)
        numeroDePixelesPorComponente.add(pixeles)
        totalPosiciones += pixeles
        if totalPosiciones == len(posiciones):
            break

def conectividadCuatro(X, Y):
    for a in [-1, 0, 1]:
        for b in [-1, 0, 1]:

            if (a == -1 & b == 0):
                newX = X + a
                newY = Y + b
                yield newX, newY

            elif (a == 1 & b == 0):
                newX = X + a
                newY = Y + b
                yield newX, newY

            elif (a == 0 & b == -1):
                newX = X + a
                newY = Y + b
                yield newX, newY

            elif (a == 0 & b == 1):
                newX = X + a
                newY = Y + b
                yield newX, newY

            elif (a == 0 & b == 0):
                newX = X + a
                newY = Y + b
                yield newX, newY

listaPerimetroDeContacto = set()

def perimetroDeContacto(sumaDePixeles):
    perimetroDeContacto = 0
    componenteContacto = set()

    sumaDePixeles += len(componenteIndividual)
    largoComponente = len(componenteIndividual)

    for (x, y) in componenteIndividual:
        if (len(componenteIndividual) == 1):
            listaPerimetroDeContacto.add(perimetroDeContacto)
            break

        for (newX, newY) in conectividadCuatro(x, y):
            if(len(componenteContacto) != 0):
                perimetroDeContacto += 1

            if (newX, newY) != (x, y):
                componenteContacto.add((newX, newY))
            else:
                componenteContacto.add((x,y))

            largoComponenteAuxiliar = len(componenteContacto)

            if (largoComponente == largoComponenteAuxiliar):
                listaPerimetroDeContacto.add(perimetroDeContacto)
                break

        if (largoComponente == largoComponenteAuxiliar):
            break

    return sumaDePixeles

listaDePerimetros = set()

def perimetroDeComponente():
    a=0
    totalPosiciones = 0
    for (x, y) in posiciones:
        validacionPorComponente(x, y)
        totalPosiciones = totalPosiciones + perimetroDeContacto(a)
        if(totalPosiciones == len(posiciones)):
            break

    pC = 0
    for x in listaPerimetroDeContacto:
        nP = 0
        for y in numeroDePixelesPorComponente:
            if (nP == pC):
                valorPerimetro = perimetroSinHoyos(y,x)
                listaDePerimetros.add(valorPerimetro)
            nP += 1
        pC += 1


def perimetroConHoyos(numeroDePixeles, perimetroDeContacto):
    perimetro = (numeroDePixeles * 4) - (2 * perimetroDeContacto) + 4
    return perimetro


def perimetroSinHoyos(numeroDePixeles, perimetroDeContacto):
    perimetro = (numeroDePixeles * 4) - (2 * perimetroDeContacto)
    return perimetro

def inicio():
    pixelesOn()
    paresDeComponentes()
    cantidadComponentes()
    numeroDePixelesEnComponentes()
    perimetroDeComponente()

def presentacionInformacion():
    x = 0
    for x in numeroDePixelesPorComponente:
        print componenteNo, " "

    for x in listaPerimetroDeContacto

    for x in listaDePerimetros

if __name__ == "__main__":
    inicio()
    presentacionInformacion()






